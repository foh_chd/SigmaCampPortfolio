let chessElement = document.getElementById('chessBoard');
for (var i = 0; i < 8; i++) {
    if (i % 2 == 1) {
        for (let n = 0; n < 8; n++) {
            if (n % 2 == 0) {
                chessElement.innerHTML += '<div class="chessElement"></div>';
            }
            if (n % 2 == 1) {
                chessElement.innerHTML += '<div class="chessElement black"></div>';
            }
        }
    }
    if (i % 2 == 0) {
        for (let m = 0; m < 8; m++) {
            if (m % 2 == 1) {
                chessElement.innerHTML += '<div class="chessElement"></div>';
            }
            if (m % 2 == 0) {
                chessElement.innerHTML += '<div class="chessElement black"></div>';
            }
        }
    }
}
