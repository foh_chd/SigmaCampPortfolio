// -------------------------------------Variables
let s = '00';
let m = '00';
let h = '00';
let hours = document.getElementById('hours');
let minutes = document.getElementById('minutes');
let seconds = document.getElementById('seconds');
let start = document.getElementById('start');
let pause = document.getElementById('pause');
let lap = document.getElementById('lap');
let refresh = document.getElementById('refresh');
let fx = document.getElementById('startFx');
let lapParams = h + ':' + m + ':' + s;
// -------------------------------------Variables
// -------------------------------------HTML Gen Variables
seconds.innerHTML = s;
minutes.innerHTML = m;
hours.innerHTML = h;
// -------------------------------------HTML Gen Variables
// -------------------------------------Fx
function visualFx(massege) {
    fx.innerHTML = massege;
    fx.classList.add("startFx__Visible");
        setTimeout(() => {
    fx.classList.remove("startFx__Visible");
        }, 200);
}
// -------------------------------------Fx
// -------------------------------------Timer
function timer () {
// -------------------------------------Seconds
    visualFx('start')
    startSeconds = setInterval(() => {
        s++
        if (s < 10) {
            seconds.innerHTML = '0' + s;
        }else{
            seconds.innerHTML = s;
        }
        if (s === 60) {
            seconds.innerHTML = '00';
            s = '00';
        }
    }, 1000);
// -------------------------------------Seconds
// -------------------------------------Minutes
    startMinutes = setInterval(() => {
        m++
        if (m < 10) {
            minutes.innerHTML = '0' + m;
        }else{
            minutes.innerHTML = m;
        }
        if (m === 60) {
            minutes.innerHTML = '00';
            m = '00';
        }
    }, 60000);
// -------------------------------------Minutes
// -------------------------------------Hours
    startHours = setInterval(() => {
        h++
        if (h < 10) {
            hours.innerHTML = '0' + h;
        }else{
            hours.innerHTML = h;
        }
        if (h === 99) {
            hours.innerHTML = '00';
            h = '00';
        }
    }, 3600000);
// -------------------------------------Hours
}
// -------------------------------------Timer
// -------------------------------------Start Fx
function startEffect() {
    setTimeout(() => {
        
    }, timeout);
}
// -------------------------------------Start Fx
// -------------------------------------Pause
function pauseTimer() {
    clearInterval(startSeconds);
    clearInterval(startMinutes);
    clearInterval(startHours);
    visualFx('pause');
}
// -------------------------------------Pause
// -------------------------------------Refresh
function refreshTimer() {
    s = '00';
    m = '00';
    h = '00';
    seconds.innerHTML = '00';
    minutes.innerHTML = '00';
    hours.innerHTML = '00';
    document.getElementById('lapWindow').innerHTML = '';
    visualFx('refresh');
}
// -------------------------------------Refresh
// -------------------------------------Lap
function lapRec() {
    visualFx('Lap');
    document.getElementById('lapWindow').innerHTML = lapParams;
}
// -------------------------------------Lap
// -------------------------------------Buttons
start.addEventListener("click", timer);
pause.addEventListener("click", pauseTimer);
refresh.addEventListener("click", refreshTimer);
lap.addEventListener("click", lapRec);
// -------------------------------------Buttons